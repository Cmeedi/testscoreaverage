﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestScoreAverage
{
    public partial class TestScoreAverageForm : Form
    {
        public TestScoreAverageForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void CalculateAverageButton_Click(object sender, EventArgs e)
        {

            try
            {
                const double HIGH_SCORE = 95.0;         // High Score value
                double test1, test2, test3, average;    // Variables

                //Get the test scores from the TestBoxes
                test1 = double.Parse(TestOneTextBox.Text);
                test2 = double.Parse(TestTwoTextBox.Text);
                test3 = double.Parse(TestThreeTextBox.Text);

                //Calculate the average test score.
                average = (test1 + test2 + test3) / 3.0;

                //Display the average, rounded to 2 decimal places.
                AverageResultLabel.Text = average.ToString("n1");

                //If the average is a high score, congratulate
                // the user with a message box.
                if(average > HIGH_SCORE)
                {
                    MessageBox.Show("CONGRATULATIONS!  Great Job!");
                }      
            }

            catch(Exception ex)
            {
                //Display the default erro message.
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            //Clear the TestBoxes and the averageLabel control.
            TestOneTextBox.Text = "";
            TestTwoTextBox.Text = "";
            TestThreeTextBox.Text = "";
            AverageLabel.Text = "";

            //Reset the focus to test1
            TestOneTextBox.Focus();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            //Close the form.
            this.Close();
        }
    }
}
