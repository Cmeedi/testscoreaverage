﻿namespace TestScoreAverage
{
    partial class TestScoreAverageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TestScoresGroupBox = new System.Windows.Forms.GroupBox();
            this.TestScoreOneLabel = new System.Windows.Forms.Label();
            this.TestScoreTwoLabel = new System.Windows.Forms.Label();
            this.TestScoreThreeLabel = new System.Windows.Forms.Label();
            this.AverageLabel = new System.Windows.Forms.Label();
            this.TestOneTextBox = new System.Windows.Forms.TextBox();
            this.TestTwoTextBox = new System.Windows.Forms.TextBox();
            this.TestThreeTextBox = new System.Windows.Forms.TextBox();
            this.AverageResultLabel = new System.Windows.Forms.Label();
            this.CalculateAverageButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.TestScoresGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // TestScoresGroupBox
            // 
            this.TestScoresGroupBox.Controls.Add(this.AverageResultLabel);
            this.TestScoresGroupBox.Controls.Add(this.TestThreeTextBox);
            this.TestScoresGroupBox.Controls.Add(this.TestTwoTextBox);
            this.TestScoresGroupBox.Controls.Add(this.TestOneTextBox);
            this.TestScoresGroupBox.Controls.Add(this.AverageLabel);
            this.TestScoresGroupBox.Controls.Add(this.TestScoreThreeLabel);
            this.TestScoresGroupBox.Controls.Add(this.TestScoreTwoLabel);
            this.TestScoresGroupBox.Controls.Add(this.TestScoreOneLabel);
            this.TestScoresGroupBox.Location = new System.Drawing.Point(12, 12);
            this.TestScoresGroupBox.Name = "TestScoresGroupBox";
            this.TestScoresGroupBox.Size = new System.Drawing.Size(347, 251);
            this.TestScoresGroupBox.TabIndex = 0;
            this.TestScoresGroupBox.TabStop = false;
            this.TestScoresGroupBox.Text = "Enter Three Test Scores";
            // 
            // TestScoreOneLabel
            // 
            this.TestScoreOneLabel.AutoSize = true;
            this.TestScoreOneLabel.Location = new System.Drawing.Point(48, 43);
            this.TestScoreOneLabel.Name = "TestScoreOneLabel";
            this.TestScoreOneLabel.Size = new System.Drawing.Size(97, 17);
            this.TestScoreOneLabel.TabIndex = 0;
            this.TestScoreOneLabel.Text = "Test Score #1";
            // 
            // TestScoreTwoLabel
            // 
            this.TestScoreTwoLabel.AutoSize = true;
            this.TestScoreTwoLabel.Location = new System.Drawing.Point(48, 77);
            this.TestScoreTwoLabel.Name = "TestScoreTwoLabel";
            this.TestScoreTwoLabel.Size = new System.Drawing.Size(97, 17);
            this.TestScoreTwoLabel.TabIndex = 1;
            this.TestScoreTwoLabel.Text = "Test Score #2";
            // 
            // TestScoreThreeLabel
            // 
            this.TestScoreThreeLabel.AutoSize = true;
            this.TestScoreThreeLabel.Location = new System.Drawing.Point(48, 114);
            this.TestScoreThreeLabel.Name = "TestScoreThreeLabel";
            this.TestScoreThreeLabel.Size = new System.Drawing.Size(97, 17);
            this.TestScoreThreeLabel.TabIndex = 2;
            this.TestScoreThreeLabel.Text = "Test Score #3";
            this.TestScoreThreeLabel.Click += new System.EventHandler(this.label2_Click);
            // 
            // AverageLabel
            // 
            this.AverageLabel.AutoSize = true;
            this.AverageLabel.Location = new System.Drawing.Point(84, 174);
            this.AverageLabel.Name = "AverageLabel";
            this.AverageLabel.Size = new System.Drawing.Size(61, 17);
            this.AverageLabel.TabIndex = 3;
            this.AverageLabel.Text = "Average";
            // 
            // TestOneTextBox
            // 
            this.TestOneTextBox.Location = new System.Drawing.Point(162, 43);
            this.TestOneTextBox.Name = "TestOneTextBox";
            this.TestOneTextBox.Size = new System.Drawing.Size(144, 22);
            this.TestOneTextBox.TabIndex = 4;
            // 
            // TestTwoTextBox
            // 
            this.TestTwoTextBox.Location = new System.Drawing.Point(162, 77);
            this.TestTwoTextBox.Name = "TestTwoTextBox";
            this.TestTwoTextBox.Size = new System.Drawing.Size(144, 22);
            this.TestTwoTextBox.TabIndex = 5;
            // 
            // TestThreeTextBox
            // 
            this.TestThreeTextBox.Location = new System.Drawing.Point(162, 114);
            this.TestThreeTextBox.Name = "TestThreeTextBox";
            this.TestThreeTextBox.Size = new System.Drawing.Size(144, 22);
            this.TestThreeTextBox.TabIndex = 6;
            // 
            // AverageResultLabel
            // 
            this.AverageResultLabel.BackColor = System.Drawing.SystemColors.Control;
            this.AverageResultLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AverageResultLabel.Location = new System.Drawing.Point(162, 174);
            this.AverageResultLabel.Name = "AverageResultLabel";
            this.AverageResultLabel.Size = new System.Drawing.Size(144, 22);
            this.AverageResultLabel.TabIndex = 7;
            // 
            // CalculateAverageButton
            // 
            this.CalculateAverageButton.Location = new System.Drawing.Point(74, 269);
            this.CalculateAverageButton.Name = "CalculateAverageButton";
            this.CalculateAverageButton.Size = new System.Drawing.Size(100, 86);
            this.CalculateAverageButton.TabIndex = 1;
            this.CalculateAverageButton.Text = "Calculate Average";
            this.CalculateAverageButton.UseVisualStyleBackColor = true;
            this.CalculateAverageButton.Click += new System.EventHandler(this.CalculateAverageButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(180, 269);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(100, 38);
            this.ClearButton.TabIndex = 2;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(180, 317);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(100, 38);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // TestScoreAverageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 367);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.CalculateAverageButton);
            this.Controls.Add(this.TestScoresGroupBox);
            this.Name = "TestScoreAverageForm";
            this.Text = "Test Score Average";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TestScoresGroupBox.ResumeLayout(false);
            this.TestScoresGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox TestScoresGroupBox;
        private System.Windows.Forms.Label TestScoreThreeLabel;
        private System.Windows.Forms.Label TestScoreTwoLabel;
        private System.Windows.Forms.Label TestScoreOneLabel;
        private System.Windows.Forms.Label AverageResultLabel;
        private System.Windows.Forms.TextBox TestThreeTextBox;
        private System.Windows.Forms.TextBox TestTwoTextBox;
        private System.Windows.Forms.TextBox TestOneTextBox;
        private System.Windows.Forms.Label AverageLabel;
        private System.Windows.Forms.Button CalculateAverageButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button ExitButton;
    }
}

